package com.example.android.theatreofdreams;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import static android.provider.AlarmClock.EXTRA_MESSAGE;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView t2 = (TextView) findViewById(R.id.hyperlink);
        t2.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void display(int year) {
        final TextView textview = (TextView) findViewById(R.id.hyperlink);
        switch (year) {
            case 2005:
                textview.setText(getString(R.string.text2005));
                break;
            case 2006:
                textview.setText(getString(R.string.text2006));
                break;
            case 2008:
                textview.setText(getString(R.string.text2008));
                break;
            case 2011:
                textview.setText(getString(R.string.text2011));
                break;
            case 2012:
                textview.setText(getString(R.string.text2012));
                break;
        }
    }

    public void click_2005(View view) {
        display(2005);
    }

    public void click_2006(View view) {
        display(2006);
    }


    public void click_2008(View view) {
        display(2008);
    }

    public void click_2011(View view) {
        display(2011);
    }

    public void click_2012(View view) {
        display(2012);
    }

    public void newsClick(View view) {
        Intent intent = new Intent(this, NewsActivity.class);
        startActivity(intent);

    }
    public void voteClick(View view) {
        Intent intent = new Intent(this, VoteActivity.class);
        startActivity(intent);
    }
}